<?php

/**
 * @file
 * Describe hooks provided by the GPX Track & Elevation module.
 */

/**
 * Modify the waypoint list.
 *
 * You can add/edit/remove waypoints by means of
 * hook_gpx_track_elevation_waypoints_alter().
 *
 * @param array $waypoints
 *   An array containing all defined waypoints that will be rendered.
 *   $waypoints elements are array with the following elements:
 *   - "name"
 *   - "latitude"
 *   - "longitude"
 *   - "description"
 *   - "elevation"
 *   - "type".
 */
function hook_gpx_track_elevation_waypoints_alter(array &$waypoints) {
  // This example describes how to add a waypoint using
  // hook_gpx_track_elevation_waypoints_modification().
  $waypoints[] = array(
    'Greenwich',
    // Waypoint name.
    51.48,
    // Latitude.
    0,
    // Longitude.
    'The Greenwich observatory',
    // Waypoint description.
    '175',
    // Waypoint elevation.
    'sights',
    // Waypoint type.
  );
}
